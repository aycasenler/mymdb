package com.example.mymdb

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageButton
import androidx.fragment.app.FragmentManager
import com.google.firebase.database.FirebaseDatabase

class MainActivity : AppCompatActivity(), View.OnClickListener {


    private var plusButton: ImageButton? = null
    private var gridButton: ImageButton? = null
    private var frameLayout: FrameLayout? = null
    private var boolean = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        initVerticalFragment()
    }

    fun init() {
        plusButton = findViewById(R.id.btn_plus)
        gridButton = findViewById(R.id.btn_grid)
        frameLayout = findViewById(R.id.frameLayout)

        plusButton?.setOnClickListener(this)
        gridButton?.setOnClickListener(this)
    }
    override fun onClick(v: View?) {
        when(v?.id){
            plusButton?.id->{
                initAddFragment()
            }
            gridButton?.id->{

                if(boolean){
                    initVerticalFragment()
                    boolean = false
                }else{
                    initGridFragment()
                    boolean = true
                }
            }

        }
    }
    fun initVerticalFragment(){
        val verticalFragment = VercitalScreenFragment()
        val transaction = supportFragmentManager.beginTransaction()
        transaction.add(R.id.frameLayout, verticalFragment,"VerticalScreenFragment")
        transaction.addToBackStack("VerticalScreenFragment")
        transaction.commit()


        
    }

    override fun onBackPressed() {
        if(supportFragmentManager.backStackEntryCount == 1){
            finish()
        }else{
            supportFragmentManager.popBackStack()
        }
    }
    fun initGridFragment(){
        val gridFragment = GridScreenFragment()
        val transaction = supportFragmentManager.beginTransaction()
        transaction.add(R.id.frameLayout, gridFragment,"GridScreenFragment")
        transaction.addToBackStack("GridScreenFragment")
        transaction.commit()
    }
    fun initAddFragment() {
        val addMovieFragment = AddMovieFragment()
        val transaction = supportFragmentManager.beginTransaction()
        transaction.add(R.id.frameLayout, addMovieFragment, "AddMovieFragment")
        transaction.addToBackStack("AddMovieFragment")
        transaction.commit()
    }
}
