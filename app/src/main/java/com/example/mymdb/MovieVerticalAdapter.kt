package com.example.mymdb

import android.content.Context
import android.os.Bundle

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import androidx.fragment.app.FragmentActivity



class MovieVerticalAdapter(val movieList: MutableList<AddMovieFragment.Movies>,val context : Context) :
    RecyclerView.Adapter<MovieVerticalAdapter.MyViewHolder>() {
    val onItemClick: ((AddMovieFragment.Movies) -> Unit)? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.movie_vertical_item, parent, false)
        return MyViewHolder(itemView)

    }


    override fun getItemCount(): Int {
        return movieList.size
    }

    fun clickMovie(item : AddMovieFragment.Movies){
        val position = movieList.indexOf(item)
        val bundle = Bundle()
        //bundle.putString("Name", movieList.get(position).name)
        bundle.putString("Name",movieList.get(position).name)
        bundle.putString("Date",movieList.get(position).date)
        bundle.putString("Detail",movieList.get(position).details)
        bundle.putString("Producer",movieList.get(position).producer)
        bundle.putString("Rate",movieList.get(position).rate)

       var detailsFragment = DetailsFragment()
        detailsFragment.arguments = bundle

        val transaction = (context as FragmentActivity).getSupportFragmentManager().beginTransaction()
        transaction.add(R.id.frameLayout, detailsFragment, "DetailsFragment")
        transaction.addToBackStack("DetailsFragment")
        transaction.commit()

    }



    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val movie = movieList[position]
        holder.txtName?.text = movie.name
        holder.txtDetails?.text = movie.details
        holder.txtRate?.text = movie.rate



        holder.itemView.setOnClickListener {
           // movie.let { it1 -> onItemClick?.invoke(it1) }
            Toast.makeText(holder.itemView.context, "${movie.name} is clicked", Toast.LENGTH_SHORT).show()

            clickMovie(movie)

        }


    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        val txtName: TextView? = itemView.findViewById(R.id.txtName)
        val txtDetails: TextView? = itemView.findViewById(R.id.txtDetails)
        val txtRate : TextView? = itemView.findViewById(R.id.txtRate)


        init{
            itemView.setOnClickListener{
                onItemClick?.invoke(movieList[adapterPosition])
            }
        }
    }

}

