package com.example.mymdb

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.grid_screen_fragment.*
import kotlinx.android.synthetic.main.movie_grid_item.*

class VercitalScreenFragment : Fragment() {

    private var movieList: MutableList<AddMovieFragment.Movies>? = null
    private var recyclerView: RecyclerView? = null
    lateinit var ref: DatabaseReference

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.vertical_screen_fragment, container, false)

        ref = FirebaseDatabase.getInstance().getReference("Movies")
        movieList = mutableListOf()
        recyclerView = view.findViewById(R.id.recyclerView)
        recyclerView?.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        recyclerView?.visibility = View.VISIBLE
        ref.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                Toast.makeText(context, "Error Occurred " + p0.toException(), Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onDataChange(p0: DataSnapshot) {
                if (p0.exists()) {
                    for (h in p0.children) {
                        val movie = h.getValue(AddMovieFragment.Movies::class.java)
                        movieList?.add(movie!!)
                    }

                    val adapter = MovieVerticalAdapter(movieList!!,context!!)

                    recyclerView?.adapter = adapter


                }
            }

        })
        return view
    }



}