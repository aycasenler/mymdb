package com.example.mymdb


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment

class DetailsFragment : Fragment() {
    private var txtName: TextView? = null
    private var txtDate: TextView? = null
    private var txtDetail: TextView? = null
    private var txtRate: TextView? = null
    private var txtProducer: TextView? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.details_fragment, container, false)
        txtName = view?.findViewById(R.id.txtName)
        txtDate = view?.findViewById(R.id.txtReleaseDate)
        txtDetail = view?.findViewById(R.id.txtDetail)
        txtRate = view?.findViewById(R.id.txtRate)
        txtProducer = view?.findViewById(R.id.txtProducer)

        txtName?.text = this.arguments?.getString("Name")
        txtDate?.text = this.arguments?.getString("Date")
        txtDetail?.text = this.arguments?.getString("Detail")
        txtRate?.text = this.arguments?.getString("Rate")
        txtProducer?.text = this.arguments?.getString("Producer")


        if(txtProducer?.text?.isEmpty()!!){
            txtProducer?.visibility = View.GONE
        }
        if(txtDetail?.text?.isEmpty()!!){
            txtDetail?.visibility = View.GONE
        }

        return view
    }
}