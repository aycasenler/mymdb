package com.example.mymdb

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class AddMovieFragment : Fragment(), View.OnClickListener {

    lateinit var ref: DatabaseReference
    private var edtName: EditText? = null
    private var edtDetails: EditText? = null
    private var edtReleaseDate: EditText? = null
    private var edtProducer: EditText? = null
    private var btnSave: Button? = null
    private var btnCancel: Button? = null
    private var edtRate: EditText? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.add_movie_fragment, container, false)
        init(view)
        return view

    }

    fun init(view: View) {
        ref = FirebaseDatabase.getInstance().getReference("Movies")
        edtName = view.findViewById(R.id.edtName)
        edtDetails = view.findViewById(R.id.edtDetail)
        edtReleaseDate = view.findViewById(R.id.edtReleaseDate)
        edtProducer = view.findViewById(R.id.edtProducer)
        btnSave = view.findViewById(R.id.btnSave)
        btnCancel = view.findViewById(R.id.btnCancel)
        edtRate = view.findViewById(R.id.edtRate)

        btnSave?.setOnClickListener(this)
        btnCancel?.setOnClickListener(this)

    }

    fun initFragment() {
        val verticalFragment = VercitalScreenFragment()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.add(R.id.frameLayout, verticalFragment, "VerticalScreenFragment")
        transaction?.addToBackStack("VerticalScreenFragment")
        transaction?.commit()
    }

    fun saveData() {
        val name = edtName?.text.toString().trim()
        if (name.isEmpty()) {
            edtName?.error = "Please enter movie name! "
            return
        }
        val movieId = ref.push().key
        val details = edtDetails?.text.toString().trim()
        val date = edtReleaseDate?.text.toString().trim()
        if (date.isEmpty()) {
            edtReleaseDate?.error = "Please enter movie release date"
            return
        }
        val producer = edtProducer?.text.toString().trim()
        val rate = edtRate?.text.toString().trim()
        if (rate.isEmpty()) {
            edtRate?.error = "Please enter movie rate"
            return
        }

        val movies = Movies(movieId!!, name, details, date, producer,rate)
        ref.child(movieId).setValue(movies).addOnCompleteListener {
            Toast.makeText(context, "Movie add successfully!", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            btnSave?.id -> {
                saveData()
            }
            btnCancel?.id -> {
                initFragment()
            }
        }
    }

    data class Movies(
        var id: String = "",
        var name: String = "",
        var details: String = "",
        var date: String = "",
        var producer: String = "",
        var rate: String = ""
    )
}
